import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class App {
    static Logger logger = Logger.getLogger(App.class.getName());
    final static String PROPERTIES = "src/main/resources/app.properties";
    final static String FILE_HANDLER = "src/main/resources/logs/";

    public static void main(String[] args) throws IOException {

        String timeStamp = new SimpleDateFormat("MM_dd__HH_mm_ss").format(new Date());
        System.out.println(timeStamp);
        FileHandler fh = new FileHandler(FILE_HANDLER + timeStamp);
        logger.addHandler(fh);
        logger.info("Start project");
        FileInputStream fis = new FileInputStream(PROPERTIES);
        Properties properties = new Properties();
        properties.load(fis);
        fis.close();
        if (fileExist(properties)) {
            logger.info("All files is exist");
            Set<String> propertiesNames = properties.stringPropertyNames();
            for (String propertieName : propertiesNames) {
                String path = properties.getProperty(propertieName);
                fileConfigRename(path);
            }
        }
        else{
            logger.info("All file is not exist");
        }
        logger.info("Finish project");
    }

    public static void fileConfigRename(String path) throws IOException {
        FileInputStream fis2 = new FileInputStream(path);
        Properties properties2 = new Properties();
        properties2.load(fis2);
        fis2.close();
        for (String properties2Name : properties2.stringPropertyNames()) {
            String newName = newName(properties2Name);
            properties2.setProperty(newName, properties2.getProperty(properties2Name));
            if (!newName.equals(properties2Name)) {
                properties2.remove(properties2Name);
                FileOutputStream fos = new FileOutputStream(path);
                properties2.store(fos, null);
                fos.close();
            }
        }
    }

    public static boolean fileExist(Properties properties) throws IOException {

        boolean exist = true;
        for (String propertieName : properties.stringPropertyNames()) {
            File file = new File(properties.getProperty(propertieName));
            if (!(file.exists())) {
                exist = false;
            }
        }
        return exist;
    }

    public static String newName(String name) {
        String[] list = name.split("_");
        StringBuilder sb = new StringBuilder();
        sb.append("NEW_NAME_");
        sb.append(list[list.length - 1]);
        logger.info("File " + name + " rename to " + sb.toString());
        return sb.toString();
    }
}